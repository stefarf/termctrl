package termctrl_test

import (
	"bitbucket.org/stefarf/termctrl"
	"fmt"
	"time"
)

func Example() {
	go func() {
		for {
			if !termctrl.Run(
				func() {
					fmt.Println("It is still running ...")
					time.Sleep(time.Second)
				}) {
				return
			}
		}
	}()

	ch := make(chan string)

	go termctrl.Run(
		func() {
			// The sender
			for {
				time.Sleep(time.Millisecond * 270)

				// Check termination signal using the Terminated bool
				if termctrl.Terminated {
					fmt.Println("[Sender] Terminated")
					return
				}

				// Check termination signal using the ChTerminate channel
				select {
				case ch <- "This is from sender":
				case <-termctrl.ChTerminate:
					fmt.Println("[Sender] ChTerminate")
					return
				}
			}
		})

	go termctrl.Run(
		func() {
			// The receiver
			for {

				// Check termination signal using the ChTerminate channel
				select {
				case s := <-ch:
					fmt.Println(s)
				case <-termctrl.ChTerminate:
					fmt.Println("[Receiver] ChTerminate")
					return
				}

			}
		})

	time.Sleep(time.Second * 2) // To give time for go routine to start
	termctrl.Wait()
	fmt.Println("Main program exited")
	time.Sleep(time.Second * 2)
}
